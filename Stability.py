#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 31 17:43:46 2017

@author: TelmaPereira and FranciscoFerreira
"""

import numpy as np
import math
from Funcoes import empty,computeMeanColumnsOfNonEmptyNestedList

# Kuncheva is defined between -1 and 1.
def KunchevaIndex(ranks,k): #each row of ranks is a rank sequence, k is the size of the top elements to consider in the rank
    IC=0
    n = len(ranks[0])
    for i in range(0,len(ranks)-1):
        for j in range(i+1,len(ranks)):
            idx1 = ranks[i][0:k]
            idx2 = ranks[j][0:k]
            intersection = np.intersect1d(idx1, idx2)
            r=len(intersection)

            IC = IC + (r*n - k**2)/(k*(n-k))
    
    stability = (2/(len(ranks)*(len(ranks)-1)))*IC
    
    return stability


def RPT(performance,robustness,beta):
    #harmonic mean of performance and robustness based on formula at pag.323 from "Robust Feature Selection Using Ensemble Feature Selection Techniques,2008"
    rpt = ((math.pow(beta,2)+1)*robustness*performance)/((math.pow(beta,2)*robustness)+performance)
    
    return rpt

def performanceBalanced(auc,sensitivity, specificity):
    perFinal=[]
    
    for i in range(0,len(auc)):
        perFinal.append(0.5*auc[i]+0.5*min(sensitivity[i],specificity[i]))
    return perFinal

def halfPerformanceStab(performance,robustness):
    #harmonic mean of performance and robustness based on formula at pag.323 from "Robust Feature Selection Using Ensemble Feature Selection Techniques,2008"
    
    beta=0.5
    
    hps = beta*robustness + (1-beta)*performance
    
    return hps

# --------------- Auxiliar functions ----------------------------------
                
def computeKunchevaSeveralK(idx_cross_validation,threshold):
    KunchevaIndexAcrossThrehold = [] 
    if not idx_cross_validation:
        # if the list is empty do nothing
        return  KunchevaIndexAcrossThrehold
    else:
                
        for t in range(len(threshold)):
        
            threshold_temp = threshold[t]
            KunchevaIndexAcrossThrehold.append(KunchevaIndex(idx_cross_validation,threshold_temp))
        
        return KunchevaIndexAcrossThrehold # for each threshold returns the stability of the given ranks


# Returns a upper diagonal matrix with the stability (averaged acroos folds) between FS methdos. 
# The input pairwiseStability has the structure [[[fold 1 FS 1], [fold 2 FS1],...,[fold n FS1]]]],[[[fold 1 FS 2], [fold 2 FS2],...,[fold n FS2]]]],...,[]]
# The FS methods entries in the matrix follows the order: [rankFisherScoreAcrossFolds,rankChiSquaredAcrossFolds,rankTScoreAcrossFolds,rankRelieFAcrossFolds,rankSVMRFEAcrossFolds,rankMIMAcrossFolds,rankCMIMAcrossFolds,rankMRMRcrossFolds,rankLL21crossFolds])
def matrixStabilityAcrossFSMethodsPerFold (pairwiseStability, nfolds,threshold):
    
    #create matrix Number of FS algorithms used * Number of FS algorithms used
    stabilityPerFoldsTemp=[[[] for l in range(len(pairwiseStability))] for ll in range(len(pairwiseStability))]
    stabilityPerFoldsFinal=[[[] for l in range(len(pairwiseStability))] for ll in range(len(pairwiseStability))]
    
    for fold in range (0,nfolds): 
        for i in range(len(pairwiseStability)):
            if empty(pairwiseStability[i]):
                pass
            else:
                rank1=pairwiseStability[i][fold]
                
                for j in range(len(pairwiseStability)):
                    
                    if empty(pairwiseStability[j]) or j<=i:
                        pass
                    else:
                        rank2=pairwiseStability[j][fold]
                        rank=[]
                        rank.extend([rank1, rank2]) # compute the stability between the two ranks produced by a FS method i and j
                        stabilityPerFoldsTemp[i][j].append(computeKunchevaSeveralK(rank,threshold))
    
 
    # Compute the mean of stability across folds
    for k in range(len(pairwiseStability)):
        for kk in range(len(pairwiseStability)):
            stabilityPerFoldsFinal[k][kk]=computeMeanColumnsOfNonEmptyNestedList(stabilityPerFoldsTemp[k][kk])   
    return stabilityPerFoldsFinal
        
        
# Other auxiloar functions
    
def sigmoid(scores):
    return 1 / (1 + np.exp(-scores))












    