#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 31 17:46:39 2017

@author: TelmaPereira and FranciscoFerreira
"""
import pandas as pd
import math

class Dados():
    
    def __init__(self,base="ADNI",janela=4):
        if base=="ADNI":
            self.base="ADNI"
   
                
            if janela==2:
                dados = pd.read_csv("TradBL_V21_ADNI_TODASFEATURES.csv")
                janela=2
            elif janela==3:
                dados = pd.read_csv("TradBL_V31_ADNI_TODASFEATURES.csv")
                janela=3
            elif janela==4:
                dados = pd.read_csv("TradBL_V41_ADNI_TODASFEATURES.csv")
                janela=4
             
        else:
            self.base="CCC"
               
            if janela==2: 
                dados = pd.read_csv("CCC_Oct17_Lis_raw_20MV_2Y.csv")
                janela=2
            elif janela==3:
                dados = pd.read_csv("CCC_Oct17_Lis_raw_20MV_3Y.csv")
                janela=3
            elif janela==4:
                dados = pd.read_csv("CCC_Oct17_Lis_raw_20MV_4Y.csv")
                janela=4
                
        self.dados = dados
        self.janela = janela
        
        if self.base=="CCC":
            self.dados.rename(columns = {'Progression':'EVOL', 'Informação_Total':'Informacao_Total', 'Informação_Total_Z':'Informacao_Total_Z'}, inplace = True)
            self.dados.drop('Case_number_for_this_database',axis=1) # nao esta a fazer porque???
            self.X = self.dados.iloc[:,1:self.dados.shape[1]-1] # drop case number              
            #self.X = self.dados.iloc[:,1:self.dados.shape[1]-3] # drop case number              
        else:
            self.X = self.dados.iloc[:,0:self.dados.shape[1]-1]
            #self.X = self.dados.iloc[:,0:self.dados.shape[1]-2]
        
        self.y = self.dados['EVOL']
        
    def description(self):
        string =  ''.join(["You are using ",self.base," database with a ",str(self.janela),"Y time-window."," There are ",str(self.dados.shape[0])," instances and ",str(self.dados.shape[1]-1)," features."," Patients EVOL: ",str(self.y.sum())," Patients noEVOL: ",str(self.dados.shape[0]-self.y.sum())])
        return string
    
    def setThreshold (self):
        nFeatures = self.dados.shape[1]-1
        t1=list(range(1,math.ceil(nFeatures/2)))
        t2=list (range (math.ceil(nFeatures/2),nFeatures,5))
        return t1 + t2 
    
        
    
        
    
        
