#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 16 17:07:24 2017

@author: TelmaPereira and FranciscoFerreira
"""



import pandas as pd
from sklearn.linear_model import LogisticRegression
from sklearn import metrics
from sklearn.model_selection import StratifiedKFold
from statistics import mean
import numpy as np
from Dados import Dados
from sklearn.naive_bayes import GaussianNB
from sklearn.preprocessing import normalize
from sklearn.preprocessing import Imputer
from Funcoes import calcSensiSpeci
from imblearn.over_sampling import SMOTE
numpy.set_printoptions(threshold=numpy.nan)

# Train models with the entire set of features to compare with the results obtained 
# when using a subset of features selected using FSE-StabPred
 
def tTimesnFoldCrossValidation(dados,t,classifier,n,smote,filename="Results"):
    
    X = dados.X
    y = dados.y
    
    accuracy_total=[]    
    medidas_threshold_across_folds=[]
    medidas_threshold_across_folds_sensi=[]
    medidas_threshold_across_folds_speci=[]
    medidas_threshold_across_folds_FM=[]
    medidas_balancedPerf_across_folds=[]
                

    if classifier == "NaiveBayes":
        classifier = GaussianNB()
    elif classifier == "LogisticRegression":
        classifier = LogisticRegression()
    elif classifier == "DecisionTree":
        classifier = DecisionTreeClassifier()
    elif classifier == "KNN":
        classifier = KNeighborsClassifier()
    elif classifier == "SVM_poly":
        classifier = svm.SVC(C=1.0,kernel='poly', degree=2, probability=True, gamma = 0.1, max_iter=10000) 
    elif classifier == "SVM_rbf":
        classifier = svm.SVC(C=1.0,kernel='rbf', probability=True, gamma = 0.1)
    
    file = open(''.join([filename,".txt"]),"w") 
    file.write(dados.description())         
    file.write(''.join(["\n\nTraining conditions: ",str(t),"x",str(n),"CV"]))

        
    if smote==1:
        file.write("\nUsing Smote? Yes!")
    else:
        file.write("\nUsing Smote? No!")
    
        
    file.write(''.join(["\n: ", "AUC ", "\t","sensitivity","\t", "specificity"]))
   
    for i in range(1,t+1):
        
        skf = StratifiedKFold(n_splits=n,shuffle=True,random_state=i)
        
        lista_cross_validation=[]
        medida_vector_AUC=[]
        sensi_speci=[]
        medida_vector_FMeasure=[]
        medida_vector_Sensi=[]
        medida_vector_Speci=[]
        medida_vector_balancedPerf=[]
        
        for train_index, test_index in skf.split(X,y):

            X_train, X_test = X.iloc[train_index,:], X.iloc[test_index,:]              
            y_train, y_test = y.iloc[train_index], y.iloc[test_index]
         
            # Forgetting endex

            if dados.base=="ADNI":
                
                    min_FORGETINDEX = X_train.FORGETINDEX.min() #Colocar o forget index a positivo
                    X_train.FORGETINDEX = X_train.FORGETINDEX-min_FORGETINDEX #Colocar o forget index a positivo
                    X_test.FORGETINDEX = X_test.FORGETINDEX-min_FORGETINDEX #Colocar o forget index a positivo

            else:
                    
                    X_train.Fi_LM_a_m100 = X_train.Fi_LM_a_m100+100
                    X_test.Fi_LM_a_m100 = X_test.Fi_LM_a_m100+100
                                    
            imputer = Imputer()
            imputer = Imputer(strategy="most_frequent") # impute missing values with the most frequent value              
            imputer.fit(X_train)
                
            X_train = pd.DataFrame(imputer.transform(X_train))              
            X_test = pd.DataFrame(imputer.transform(X_test))
                
    
            if smote==1:
                smote = SMOTE()
                smote.fit(X_train,y_train)
                X_train,y_train = smote.fit_sample(X_train, y_train)
                    
            
            #train
            classifier.fit(X_train,y_train)
            
            y_pred = classifier.predict(X_test)
            
            lista_cross_validation.append(metrics.accuracy_score(y_test,y_pred))
            
            y_pred_prob = classifier.predict_proba(X_test)[:, 1]
            


            sensi_speci = calcSensiSpeci(metrics.confusion_matrix(y_test,y_pred,labels=[1,0])) #labels [1,0] define positive class (=1).
                    
            medida_vector_AUC.append(metrics.roc_auc_score(y_test, y_pred_prob, average='weighted'))
                    
            medida_vector_FMeasure.append(metrics.f1_score(y_test,y_pred,pos_label=1,average='weighted')) # pos_label =1 
                    
            medida_vector_Sensi.append(sensi_speci[0])
                    
            medida_vector_Speci.append(sensi_speci[1])
            
            auc=metrics.roc_auc_score(y_test, y_pred_prob, average='weighted')
            medida_vector_balancedPerf.append(0.5*auc+0.5*min(sensi_speci[0],sensi_speci[1]))
            
        

        
        medidas_threshold_across_folds.append(mean(medida_vector_AUC)) # keeps the AUC of each fold and for each threshold for the actual seed/times
                
        medidas_threshold_across_folds_sensi.append(mean(medida_vector_Sensi))
                
        medidas_threshold_across_folds_speci.append(mean(medida_vector_Speci))
                
        medidas_threshold_across_folds_FM.append(mean(medida_vector_FMeasure))
        
        medidas_balancedPerf_across_folds.append(mean(medida_vector_balancedPerf))
        
        accuracy_total.append(mean(lista_cross_validation))
        
    print("Accuracy:", mean(accuracy_total))
    print("AUC:", mean(medidas_threshold_across_folds))
    print("Sensitivity:", mean(medidas_threshold_across_folds_sensi))
    print("Specificity:", mean(medidas_threshold_across_folds_speci))
    print("F-Measure:", mean(medidas_threshold_across_folds_FM))
    
    
    file.write(''.join(["\n Mean: ", "AUC ", str(mean(medidas_threshold_across_folds))," Sensitivity ",str( mean(medidas_threshold_across_folds_sensi))," Specificity ", str(mean(medidas_threshold_across_folds_speci)), " Balanced performance ", str(mean(medidas_balancedPerf_across_folds))]))
    file.write(''.join(["\n STD: ", "AUC ", str(np.std(medidas_threshold_across_folds))," Sensitivity ",str( np.std(medidas_threshold_across_folds_sensi))," Specificity ", str(np.std(medidas_threshold_across_folds_speci))," Balanced performance ", str(np.std(medidas_balancedPerf_across_folds))]))
    file.write(''.join(["\n\n ", "AUC ", str(medidas_threshold_across_folds),"\t", "\n\n " , " Sensitivity", str( medidas_threshold_across_folds_sensi),"\t","\n\n ", "Specificity ", str(medidas_threshold_across_folds_speci),"\t","\n\n ", " Balanced performance", str(medidas_balancedPerf_across_folds)]))
    
    file.write(" \n balanced performance computed with AUC, sensitivity and specificity")
    for i in range(0,len(medidas_balancedPerf_across_folds)):
        file.write(''.join(["\n" , str(medidas_balancedPerf_across_folds[i])]))
    
    
    file.write(" \n AUC")
    for i in range(0,len(medidas_balancedPerf_across_folds)):
        file.write(''.join(["\n" , str(medidas_threshold_across_folds[i])]))
        
    
# RUN: 
    
dados = Dados("ADNI",4) 
#tTimesnFoldCrossValidation(dados,seeds=10,"NaiveBayes",n=5,somte=0,"filenameToOutout")
tTimesnFoldCrossValidation(dados,10,"LogisticRegression",5,1,"ADNI_2Y_noFS_SMOTE_LR")





