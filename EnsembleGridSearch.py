#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 11 10:47:36 2018

@author: TelmaPereira and FranciscoFerreira
"""

from skfeature.function.similarity_based import fisher_score
from skfeature.function.statistical_based import chi_square
from skfeature.function.statistical_based import t_score
from skfeature.function.statistical_based import f_score
from skfeature.function.similarity_based import reliefF
from skfeature.function.information_theoretical_based import MIM
from skfeature.function.information_theoretical_based import MRMR
from skfeature.function.information_theoretical_based import CMIM
from skfeature.utility.sparse_learning import *
from skfeature.function.sparse_learning_based import ll_l21

import pandas as pd
import numpy as np
from imblearn.over_sampling import SMOTE
from sklearn.linear_model import LogisticRegression
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn import svm
from sklearn.metrics import f1_score
from sklearn import preprocessing
from sklearn import metrics
from sklearn.model_selection import StratifiedKFold
from statistics import mean
from Agreggators import GeomMeanWeightAgregattor,MeanRankAgregattor,MinRankAgregattor,MaxRankAgregattor,MeanWeightAgregattor,MinWeightAgregattor,MaxWeightAgregattor,MeanRankAgregattorScore,SumRankPosAcrossFolds, normalizeWeights, getPositionOneArrayInAnother
from Stability import KunchevaIndex,RPT,computeKunchevaSeveralK,matrixStabilityAcrossFSMethodsPerFold,halfPerformanceStab,performanceBalanced
from sklearn.preprocessing import Imputer
from Funcoes import calcSensiSpeci,computeMeanColumnsOfNonEmptyNestedList,empty,transformWeight
import copy
import math
from sklearn.feature_selection import RFE
from sklearn.svm import SVR
import matplotlib.pyplot as plt
import operator

class ensembleFS_GridSearchClassifiers():
    
    def defineFSmethods(self,FSmethods):
        self.FSmethods = FSmethods        
    
    def defineClassifiers(self,classifiers):
        self.classifiers = classifiers 
        
    def __init__(self,dados,n_folds=10,t_times=1,FSmethods=['t_score','reliefF'],threshold=[7],agregattor="weight_mean",classifiers=['LogisticRegression'],smote=0,filename="Results"):
        self.dados = dados
        self.n_folds = n_folds
        self.t_times = t_times # number of seeds
        self.defineClassifiers(classifiers)
        self.classifier = GaussianNB()
        self.defineThreshold(threshold)
        self.defineFSmethods(FSmethods)
        self.smote=smote
        self.filename=filename
        self.defineAgregattor(agregattor)

                   
    # define the number of features to keep in a ranking        
    def defineThreshold(self,threshold):
        threshold_aux = threshold
        for i in range(len(threshold_aux)):           
            if threshold_aux[i]=="log":
                threshold_aux[i]=int(round(math.log(self.dados.X.shape[1],2))) # log2(n), n is the total number of features
            elif isinstance(threshold_aux[i],float):
                threshold_aux[i]=int(round(self.dados.X.shape[1]*threshold_aux[i])) # percentage of the total number of features
        self.threshold = threshold_aux
            
    def defineAgregattor(self,agregattor):
        self.agregattor = agregattor
        

    def train(self):
        
        gridSearch = [[[] for nperformance in range(3)] for nclassifiers in range(len(self.classifiers))] # keep values to select the best classifiers. [[[AUC, SENSI, SPECI, STABILITY, AUC COMBI] * 3 beta values (RPT)] * NUMBER OF CLASSIFIERS TESTED]
        gridSearchAUCComb = [[[] for nperformance in range(3)] for nclassifiers in range(len(self.classifiers))] # keep values to select the best classifiers. [[[AUC, SENSI, SPECI, STABILITY, AUC COMBI] * 3 beta values (RPT)] * NUMBER OF CLASSIFIERS TESTED]
        
        # File with grid search results
        fileAux3 = open(''.join([self.filename,"gridSearch.txt"]),"w") 
        fileAux3.write(self.dados.description()) 
        
        fileAux3.write(''.join(["\n\nTraining conditions: ",str(self.t_times),"x",str(self.n_folds),"CV"]))
        fileAux3.write(''.join(["\nFS Methods: ",' '.join(self.FSmethods)]))
        fileAux3.write(''.join(["\nAgregattor: ",self.agregattor]))
        fileAux3.write(''.join(["\nThresholds: ",str(self.threshold)]))

        if self.smote==1:
            fileAux3.write("\nUsing Smote? Yes!")
        else:
            fileAux3.write("\nUsing Smote? No!")
        
        
        for c in range(len(self.classifiers)):
            classifier = self.classifiers[c]
            if classifier == "LogisticRegression":
                self.classifier = LogisticRegression()
            elif classifier == "DecisionTree":
                self.classifier = DecisionTreeClassifier()
            elif classifier == "KNN":
                self.classifier = KNeighborsClassifier()
            elif classifier == "NaiveBayes":
                self.classifier = GaussianNB()
            elif classifier == "SVM_poly":
                self.classifier = svm.SVC(C=1.0,kernel='poly', degree=2, probability=True, gamma = 0.1, max_iter=10000) # we have to add max_iterations because if not the rule is until coversion and it never ends
            elif classifier == "SVM_rbf":
                self.classifier = svm.SVC(C=1.0,kernel='rbf', probability=True, gamma = 0.1)
            self.textclassifier = classifier    
        
            
            fileAux3.write(''.join(["\n\n Classifier : ", self.textclassifier]))
           
            
            X = self.dados.X
            y = self.dados.y
        
            np.set_printoptions(threshold=np.nan)
        
            self.namechosenfeatures=[[] for listas in range(self.t_times)] # keeps the name of the features selected for each seed/time
            self.featuresrank=[[] for ntimes in range(self.t_times)] # keeps the rank of features selected for each fold in each seed/time 
            self.featuresscores=[[] for ntimes in range(self.t_times)] # keeps the scores of features selected for each fold in each seed/time
            featuresRankPosition=[[] for ntimes in range(self.t_times)] # keeps the raking position of features selected for each fold in each seed/time - to compute individual feature worth
        
            self.featuresrankTimes = []
            self.featuresscoresTimes = []
            featuresRankPosTimes = []
            self.foldsArrange=[] # keeps the folds used in each seed (t_times)
            self.trainedclassifiers=[[] for ntimes in range(self.t_times)]
        
            # File with results from the FS ensemble approach combining predictability and stability
            file = open(''.join([self.filename,self.textclassifier,"results.txt"]),"w") 
            file.write(self.dados.description()) 
        
            file.write(''.join(["\n\nTraining conditions: ",str(self.t_times),"x",str(self.n_folds),"CV"]))
            file.write(''.join(["\nClassifier: ",self.textclassifier]))
            file.write(''.join(["\nFS Methods: ",' '.join(self.FSmethods)]))
            file.write(''.join(["\nAgregattor: ",self.agregattor]))
            file.write(''.join(["\nThresholds: ",str(self.threshold)]))
        
            if self.smote==1:
                file.write("\nUsing Smote? Yes!")
            else:
                file.write("\nUsing Smote? No!")
            
            file.write("\n\n..........Training......")
        
            #File with results obtained with individual(base) FS methods 
            fileAux = open(''.join([self.filename,self.textclassifier,"_resultsIndividualFS.txt"]),"w") 
            fileAux.write(self.dados.description()) 
        
            fileAux.write(''.join(["\n\nTraining conditions: ",str(self.t_times),"x",str(self.n_folds),"CV"]))
            fileAux.write(''.join(["\nClassifier: ",self.textclassifier]))
            fileAux.write(''.join(["\nFS Methods: ",' '.join(self.FSmethods)]))
            fileAux.write(''.join(["\nAgregattor: ",self.agregattor]))
            fileAux.write(''.join(["\nThresholds: ",str(self.threshold)]))

            if self.smote==1:
                fileAux.write("\nUsing Smote? Yes!")
            else:
                fileAux.write("\nUsing Smote? No!")


            #File with classification performance and stability per seed (repetition)
            fileAux2 = open(''.join([self.filename,self.textclassifier,"_AUC_kun_perSeed.txt"]),"w") 
            fileAux2.write(self.dados.description()) 
            
            fileAux2.write(''.join(["\n\nTraining conditions: ",str(self.t_times),"x",str(self.n_folds),"CV"]))
            fileAux2.write(''.join(["\nClassifier: ",self.textclassifier]))
            fileAux2.write(''.join(["\nFS Methods: ",' '.join(self.FSmethods)]))
            fileAux2.write(''.join(["\nAgregattor: ",self.agregattor]))
            fileAux2.write(''.join(["\nThresholds: ",str(self.threshold)]))

            if self.smote==1:
                fileAux2.write("\nUsing Smote? Yes!")
            else:
                fileAux2.write("\nUsing Smote? No!")

   
            medidas_AUC_threshold_across_folds=[] # keeps the AUC values averaged across folds for each threshold
            print ("classifier ", classifier , "vector ",medidas_AUC_threshold_across_folds )
            medidas_Sensi_threshold_across_folds=[]
            medidas_Speci_threshold_across_folds=[]
            medidas_Fmeasure_threshold_across_folds=[]
            KunchevaIndexAcrossThresholdsAcrossTimes = []
        
            # Keeps the pairwise stability between FS methods across folds and across times
            stabilityPerFoldsPerTimes=[[[] for l in range(9)] for ll in range(9)] 
        
            # Keeps the stability of the same FS algorithm across folds and then averaged across times for different threshold values - list of lists
            kunchevaSameFSMethodAcrossTimesFisher = []
            kunchevaSameFSMethodAcrossTimesChiSq = []
            kunchevaSameFSMethodAcrossTimesTScore = []
            kunchevaSameFSMethodAcrossTimesRelief = []
            kunchevaSameFSMethodAcrossTimesSVMRFA = []
            kunchevaSameFSMethodAcrossTimesMIM = []
            kunchevaSameFSMethodAcrossTimesCMIM = []
            kunchevaSameFSMethodAcrossTimesMRMR = []
            kunchevaSameFSMethodAcrossTimesLL21 = []
        
        
            for i in range(1,self.t_times+1): # for each seed
        
                file.write(''.join(["\n\n\n\n\n....Seed (repetition): ",str(i)]))

                skf = StratifiedKFold(n_splits=self.n_folds,shuffle=True,random_state=i)
                
                self.foldsArrange.append(skf)
            
                idx_cross_validation=[] # keeps the rank obtained for each fold
                scores_cross_validation=[] # keeps the scores obtained for each fold
                metrics_cross_validation=[]
                rankPosition_cross_validation=[] # vector that has in each position (corresponding to a feature) the position on the ranking for that fold
            
                fold=0
            
                medidas_threshold_across_folds=[] # keeps the AUC of each threshold
                medidas_threshold_across_folds_sensi = [] # keeps the Sensibility of each threshold
                medidas_threshold_across_folds_speci = [] # keeps the Specificity of each threshold
                medidas_threshold_across_folds_FM = [] # keeps the F-measure of each threshold

                # Keeps the rank produced by each classifier in each fold. The ideia is to see how stable they are across folds             
                rankFisherScoreAcrossFolds = []
                rankChiSquaredAcrossFolds = []
                rankTScoreAcrossFolds = []
                rankRelieFAcrossFolds = []
                rankSVMRFEAcrossFolds = []
                rankMIMAcrossFolds = []
                rankCMIMAcrossFolds = []
                rankMRMRcrossFolds = []
                rankLL21crossFolds = []
            
                pairwiseStability=[]
                pairwiseStability.extend([rankFisherScoreAcrossFolds,rankChiSquaredAcrossFolds,rankTScoreAcrossFolds,rankRelieFAcrossFolds,rankSVMRFEAcrossFolds,rankMIMAcrossFolds,rankCMIMAcrossFolds,rankMRMRcrossFolds,rankLL21crossFolds])
     

                for train_index, test_index in skf.split(X,y): # for each CV fold    
                
                    file.write(''.join(["\n\n Fold: ",str(fold+1)]))

                    X_train, X_test = X.iloc[train_index,:], X.iloc[test_index,:]              
                    y_train, y_test = y.iloc[train_index], y.iloc[test_index]
                
                    if self.dados.base=="ADNI":
                
                        min_FORGETINDEX = X_train.FORGETINDEX.min() 
                        X_train.FORGETINDEX = X_train.FORGETINDEX-min_FORGETINDEX 
                        X_test.FORGETINDEX = X_test.FORGETINDEX-min_FORGETINDEX 

                    elif self.dados.base=="CCC":
                    
                        X_train.Fi_LM_a_m100 = X_train.Fi_LM_a_m100+100
                        X_test.Fi_LM_a_m100 = X_test.Fi_LM_a_m100+100
                                    
                    imputer = Imputer(strategy="most_frequent") # impute missing values with the most frequent value              
                    imputer.fit(X_train)
                
                    X_train = pd.DataFrame(imputer.transform(X_train))              
                    X_test = pd.DataFrame(imputer.transform(X_test))
                    
                    scoresFS = [] # weights attributed to each features
                    idxFS = [] # features are ranked according to the assigned weights 
                
                    # All FS methods return a ranked list of selected features
                    for j in range(len(self.FSmethods)):
                        if self.FSmethods[j]=="fisher_score":
                            scoresFS.append(fisher_score.fisher_score(X_train.as_matrix(),y_train.as_matrix()))
                            idxFS.append(fisher_score.feature_ranking(fisher_score.fisher_score(X_train.as_matrix(),y_train.as_matrix())))
                            rankFisherScoreAcrossFolds.append(fisher_score.feature_ranking(fisher_score.fisher_score(X_train.as_matrix(),y_train.as_matrix())))
                        
                        elif self.FSmethods[j]=="chi_square":
                            scoresFS.append(chi_square.chi_square(X_train.as_matrix(),y_train.as_matrix()))
                            idxFS.append(chi_square.feature_ranking(chi_square.chi_square(X_train.as_matrix(),y_train.as_matrix())))
                            rankChiSquaredAcrossFolds.append(chi_square.feature_ranking(chi_square.chi_square(X_train.as_matrix(),y_train.as_matrix())))
                        
                        elif self.FSmethods[j]=="t_score":
                            scoresFS.append(t_score.t_score(X_train.as_matrix(),y_train.as_matrix()))
                            idxFS.append(t_score.feature_ranking(t_score.t_score(X_train.as_matrix(),y_train.as_matrix())))
                            rankTScoreAcrossFolds.append(t_score.feature_ranking(t_score.t_score(X_train.as_matrix(),y_train.as_matrix())))
                        
                        elif self.FSmethods[j]=="f_score": # i am not couting with this
                            scoresFS.append(f_score.f_score(X_train.as_matrix(),y_train.as_matrix()))
                            #print(scoresFS[len(scoresFS)-1])
                            idxFS.append(f_score.feature_ranking(f_score.f_score(X_train.as_matrix(),y_train.as_matrix())))
      
                        elif self.FSmethods[j]=="reliefF":
                            scoresFS.append(reliefF.reliefF(X_train.as_matrix(),y_train.as_matrix()))
                            idxFS.append(reliefF.feature_ranking(reliefF.reliefF(X_train.as_matrix(),y_train.as_matrix())))
                            rankRelieFAcrossFolds.append(reliefF.feature_ranking(reliefF.reliefF(X_train.as_matrix(),y_train.as_matrix())))
                        
                        elif self.FSmethods[j]=="SVM-RFE":
                            estimator = SVR(kernel="linear")
                            selector = RFE(estimator, 1, step=1)
                            selector = selector.fit(X_train, y_train)
                            rankRFE = np.argsort(selector.ranking_)
                            rankRFE = rankRFE[::-1] 
                            idxFS.append(rankRFE)
                            rankSVMRFEAcrossFolds.append(rankRFE)
                            scoresFS.append(rankRFE)
                            
                        elif self.FSmethods[j]=="MIM":              
                            rank_temp=MIM.mim(X_train.as_matrix(),y_train.as_matrix(),n_selected_features=X_train.shape[1])
                            idxFS.append(rank_temp[0])
                            scoresFS.append(transformWeight(rank_temp[1],rank_temp[0]))
                            rankMIMAcrossFolds.append(rank_temp[0])
                        elif self.FSmethods[j]=="CMIM":
                            rank_temp=CMIM.cmim(X_train.as_matrix(),y_train.as_matrix(),n_selected_features=X_train.shape[1])
                            idxFS.append(rank_temp[0])
                            scoresFS.append(transformWeight(rank_temp[1],rank_temp[0]))
                            #print(scoresFS[len(scoresFS)-1])
                            rankCMIMAcrossFolds.append(rank_temp[0])
                        elif self.FSmethods[j]=="MRMR":
                            rank_temp=MRMR.mrmr(X_train.as_matrix(),y_train.as_matrix(),n_selected_features=X_train.shape[1])                      
                            rank_temp2=rank_temp[0]   
                            rank_temp2=rank_temp2[::-1]  
                            idxFS.append(rank_temp2)
                            scoresFS.append(transformWeight(rank_temp[1],rank_temp[0])*-1) 
                            rankMRMRcrossFolds.append(rank_temp2)  
                        elif self.FSmethods[j]=="LL21":
                            Y_temp = construct_label_matrix_pan(y_train)
                            Weight, obj, value_gamma = ll_l21.proximal_gradient_descent(X_train, Y_temp, 0.1)
                            idxFS.append(feature_ranking(Weight))
                            rankLL21crossFolds.append(feature_ranking(Weight))
                            scoresFS.append(Weight[:,1])
                
             
                     # aggregator  (aggregates FS rankings from multiple FS methods)      
                    if self.agregattor == "weight_mean":
                         idx=MeanWeightAgregattor(scoresFS)[0]
                         scores=MeanWeightAgregattor(scoresFS)[1]
                    elif self.agregattor == "weight_min":
                        idx=MinWeightAgregattor(scoresFS)[0]
                        scores=MinWeightAgregattor(scoresFS)[1]
                    elif self.agregattor == "weight_max":
                        idx=MaxWeightAgregattor(scoresFS)[0]
                        scores=MaxWeightAgregattor(scoresFS)[1]
                    elif self.agregattor == "weight_geom":
                        idx=GeomMeanWeightAgregattor(scoresFS)[0]
                    elif self.agregattor == "rank_mean":
                        idx=MeanRankAgregattor(idxFS)
                        rankingPos=MeanRankAgregattorScore(idxFS)
                        scores=MeanWeightAgregattor(scoresFS)[1]    
                    elif self.agregattor == "rank_min":
                        idx=MinRankAgregattor(idxFS)
                        scores=MinWeightAgregattor(scoresFS)[1]
                    elif self.agregattor == "rank_max":
                        idx=MaxRankAgregattor(idxFS)
                        scores=MaxWeightAgregattor(scoresFS)[1]
 
                    # keeps the classification performance of each threshold 
                    medida_vector_AUC =[] 
                    medida_vector_Sensi = []
                    medida_vector_Speci = []
                    medida_vector_FMeasure= []

                
                    for t in range(len(self.threshold)): # runs the classifier with t (1 to threshold) features belonging to the final rank and keeps classification performance
                

                        threshold_temp = self.threshold[t]
                       
                        features = idx[0:threshold_temp]
                        X_train_temp = X_train.iloc[:,features]
                        X_test_temp = X_test.iloc[:,features]
                        y_train_temp = y_train
                    
                    
                        if self.smote==1:
                            smote = SMOTE(random_state=1)
                            smote.fit(X_train_temp,y_train)
                            X_train_temp,y_train_temp = smote.fit_sample(X_train_temp, y_train_temp)
                        
                        
                        # If we use SVM, we normalize data
                        if self.textclassifier=="SVM_poly" or self.textclassifier=="SVM_rbf" :
                        
                            min_max_scaler = preprocessing.MinMaxScaler()
                            min_max_scaler.fit(X_train_temp)    
                            X_train_temp = pd.DataFrame(min_max_scaler.transform(X_train_temp))
                            X_test_temp = pd.DataFrame(min_max_scaler.transform(X_test_temp))
                    

                        self.classifier.fit(X_train_temp,y_train_temp)

                        y_pred = self.classifier.predict(X_test_temp)
                                    
                        y_pred_prob = self.classifier.predict_proba(X_test_temp)[:, 1]
                    
                        sensi_speci = calcSensiSpeci(metrics.confusion_matrix(y_test,y_pred,labels=[1,0])) #labels [1,0] define que a classe positiva é o 1.  
                        medida_vector_AUC.append(metrics.roc_auc_score(y_test, y_pred_prob, average='weighted'))     
                        medida_vector_FMeasure.append(metrics.f1_score(y_test,y_pred,pos_label=1,average='weighted')) # pos_label =1 define que a classe positiva é a numero 1      
                        medida_vector_Sensi.append(sensi_speci[0])
                        medida_vector_Speci.append(sensi_speci[1])                  
                
                    # keeps the classification performance of each fold and for each threshold for the actual seed(times)
                    medidas_threshold_across_folds.append(medida_vector_AUC)         
                    medidas_threshold_across_folds_sensi.append(medida_vector_Sensi)       
                    medidas_threshold_across_folds_speci.append(medida_vector_Speci)        
                    medidas_threshold_across_folds_FM.append(medida_vector_FMeasure)
                
                    file.write(''.join(["\nFeature ranking across FS methods: \n",''.join(str(idx))])) #write the aggregated rank of features of a given fold
                  
                    self.featuresrank[i-1].append(idx)
                    self.featuresscores[i-1].append(scores)
                    featuresRankPosition[i-1].append(rankingPos)
                    fold+=1
                
                    if len(self.FSmethods)>0:
                        idx_cross_validation.append(idx) # keeps the aggregated rank obtained for each fold
                        scores_cross_validation.append(scores)

                # End for - fold CV
            
                # Compute the stability achieved by each FS algorithm across the cross validation folds, per threshold 
                kunchevaSameFSMethodAcrossTimesFisher.append(computeKunchevaSeveralK(rankFisherScoreAcrossFolds,self.threshold))
                kunchevaSameFSMethodAcrossTimesChiSq.append(computeKunchevaSeveralK(rankChiSquaredAcrossFolds,self.threshold))
                kunchevaSameFSMethodAcrossTimesTScore.append(computeKunchevaSeveralK(rankTScoreAcrossFolds,self.threshold))
                kunchevaSameFSMethodAcrossTimesRelief.append(computeKunchevaSeveralK(rankRelieFAcrossFolds,self.threshold))
                kunchevaSameFSMethodAcrossTimesSVMRFA.append(computeKunchevaSeveralK(rankSVMRFEAcrossFolds,self.threshold))
                kunchevaSameFSMethodAcrossTimesMIM.append(computeKunchevaSeveralK(rankMIMAcrossFolds,self.threshold))
                kunchevaSameFSMethodAcrossTimesCMIM.append(computeKunchevaSeveralK(rankCMIMAcrossFolds,self.threshold))
                kunchevaSameFSMethodAcrossTimesMRMR.append(computeKunchevaSeveralK(rankMRMRcrossFolds,self.threshold))
                kunchevaSameFSMethodAcrossTimesLL21.append(computeKunchevaSeveralK(rankLL21crossFolds,self.threshold))
            
                stabilityPerFoldsFinal=[[[] for l in range(len(pairwiseStability))] for ll in range(len(pairwiseStability))]
                stabilityPerFoldsFinal=matrixStabilityAcrossFSMethodsPerFold(pairwiseStability, self.n_folds,self.threshold)
                fileAux.write(''.join(["\n\n....Seed/Time: ",str(i)]))
                fileAux.write(''.join(["\nKuncheva Index Across Folds for every threshold:"]))
                fileAux.write(''.join(["FS0:Fisher, FS1: ChiSquared, FS2: TScore, FS3: RelieF, FS4: SVMRFE, FS5:MIM, FS6: CMIM, FS7: MRMR, FS8: LL21"]))
            
                # Write matrixStabilityAcrossFSMethodsPerFold to a file
                for k in range(len(pairwiseStability)):
                    for kk in range(len(pairwiseStability)):
                        fileAux.write(''.join(["\n FS",str(k),", FS",str(kk), " : ",''.join(str(stabilityPerFoldsFinal[k][kk]))]))
                        stabilityPerFoldsPerTimes[k][kk].append(stabilityPerFoldsFinal[k][kk])
           
    
                # Write to a file the rankings with k features of each FS method in each fold
                k_print=15 # print the first k features
                for fold in range (0,self.n_folds): 
                    fileAux.write(''.join(["\nFold: ",''.join(str(fold))]))
                    for fsmethod in range(len(pairwiseStability)):
                        rank=[]
                        if empty(pairwiseStability[fsmethod]):
                            pass
                        else:
                            fileAux.write(''.join(["\nFS: ",''.join(str(fsmethod))]))
                            rank=pairwiseStability[fsmethod][fold]
                            feat = rank[0:k_print]
                            fileAux.write(''.join(["\nFeatures chosen: ",''.join(str(feat))]))
                            fileAux.write(''.join(["\nName features chosen: ",' '.join(self.dados.X.columns[feat].get_values())]))

                # keeps the classification performance per seed ( averaged fold for each threshold )
                medidas_AUC_threshold_across_folds.append(np.mean(medidas_threshold_across_folds,axis=0))
                medidas_Sensi_threshold_across_folds.append(np.mean(medidas_threshold_across_folds_sensi,axis=0))
                medidas_Speci_threshold_across_folds.append(np.mean(medidas_threshold_across_folds_speci,axis=0))
                medidas_Fmeasure_threshold_across_folds.append(np.mean(medidas_threshold_across_folds_FM,axis=0))

                # aggregator  (aggregates rankings from multiple folds (already combined into a final one from multille FS methods))
                if self.agregattor == "rank_mean":
                    featureRankingAcrossFolds=MeanRankAgregattor(self.featuresrank[i-1])
                    featureRankPosiAcrossFolds=SumRankPosAcrossFolds(featuresRankPosition[i-1])
                    featureScoresAcrossFolds=MeanWeightAgregattor(self.featuresscores[i-1])[1]
                elif self.agregattor == "weight_mean":
                    featureRankingAcrossFolds=MeanWeightAgregattor(self.featuresscores[i-1])[0]
                    featureScoresAcrossFolds=MeanWeightAgregattor(self.featuresscores[i-1])[1]
                elif self.agregattor == "rank_min":
                    featureRankingAcrossFolds=MinRankAgregattor(self.featuresrank[i-1])
                    featureScoresAcrossFolds=MinWeightAgregattor(self.featuresscores[i-1])[1]
                elif self.agregattor == "weight_min":
                    featureRankingAcrossFolds=MinWeightAgregattor(self.featuresscores[i-1])[0]
                    featureScoresAcrossFolds=MinWeightAgregattor(self.featuresscores[i-1])[1]
                elif self.agregattor == "rank_max":
                    featureRankingAcrossFolds=MaxRankAgregattor(self.featuresrank[i-1])
                    featureScoresAcrossFolds=MaxWeightAgregattor(self.featuresscores[i-1])[1]
                elif self.agregattor == "weight_max":
                    featureRankingAcrossFolds=MaxWeightAgregattor(self.featuresscores[i-1])[0]
                    featureScoresAcrossFolds=MaxWeightAgregattor(self.featuresscores[i-1])[1]
            
            
                top15FeaturesAcrossFolds = featureRankingAcrossFolds[0:15] 
            
                self.featuresrankTimes.append(featureRankingAcrossFolds)
                featuresRankPosTimes.append(featureRankPosiAcrossFolds)
                self.featuresscoresTimes.append(featureScoresAcrossFolds)
            
            
                file.write(''.join(["\n\nResults for seed (repetition): ",str(i)]))            
                file.write(''.join(["\nFeature ranking Across Folds: \n",''.join(str(featureRankingAcrossFolds))]))
                file.write(''.join(["\nTop 15 features Across Folds: ",' '.join(self.dados.X.columns[top15FeaturesAcrossFolds].get_values())]))
            
            
                # to each threshold compute the stability across the aggregator ranks produced in each fold 
                if len(self.FSmethods)>0:
                    KunchevaIndexAcrossThrehold = []
                    for t in range(len(self.threshold)):
                        threshold_temp = self.threshold[t]
                        KunchevaIndexAcrossThrehold.append(KunchevaIndex(idx_cross_validation,threshold_temp))
                    KunchevaIndexAcrossThresholdsAcrossTimes.append(KunchevaIndexAcrossThrehold)
            
                
         #end for seed/time       
        
        
            # Write matrixStabilityAcrossFSMethodsPerFold to a file
            fileAux.write("\n\n Pairwise Kuncheva Index Across Folds and Across Times/seeds for every threshold:FS0:Fisher, FS1: ChiSquared, FS2: TScore, FS3: RelieF, FS4: SVMRFE, FS5:MIM, FS6: CMIM, FS7: MRMR, FS8: LL21")
            for k in range(len(stabilityPerFoldsPerTimes)):
                for kk in range(len(stabilityPerFoldsPerTimes)):
                    #fileAux.write(''.join(["\n FS ",str(k),", FS ",str(kk), " : "," mean: ",str(np.mean(computeMeanColumnsOfNonEmptyNestedList(stabilityPerFoldsPerTimes[k][kk]),axis=0)), " std: ", str(np.std(computeMeanColumnsOfNonEmptyNestedList(stabilityPerFoldsPerTimes[k][kk]),axis=0)),''.join(str(computeMeanColumnsOfNonEmptyNestedList(stabilityPerFoldsPerTimes[k][kk])))]))
                    fileAux.write(''.join(["\n FS ",str(k),", FS ",str(kk), " : "," mean: ",str(np.mean(computeMeanColumnsOfNonEmptyNestedList(stabilityPerFoldsPerTimes[k][kk]))), " std: ", str(np.std(computeMeanColumnsOfNonEmptyNestedList(stabilityPerFoldsPerTimes[k][kk]))),''.join(str(computeMeanColumnsOfNonEmptyNestedList(stabilityPerFoldsPerTimes[k][kk])))]))
                   
            medidas_AUC_threshold_across_folds_times=[] # Keeps the AUC averaged across times (computed in each fold) for each threshold 
            medidas_Sensi_threshold_across_folds_times=[] # Keeps the Sensitivity averaged across times (computed in each fold) for each threshold 
            medidas_Speci_threshold_across_folds_times=[] # Keeps the Specificity averaged across times (computed in each fold) for each threshold 
            medidas_FMeasure_threshold_across_folds_times=[]
        
            medidas_AUC_threshold_across_folds_times=np.mean(medidas_AUC_threshold_across_folds,axis=0)  
            medidas_Sensi_threshold_across_folds_times=np.mean(medidas_Sensi_threshold_across_folds,axis=0) 
            medidas_Speci_threshold_across_folds_times=np.mean(medidas_Speci_threshold_across_folds,axis=0) 
            medidas_FMeasure_threshold_across_folds_times=np.mean(medidas_Fmeasure_threshold_across_folds,axis=0)
            medidas_AUC_comb_thresholds_across_folds = [[] for ntimes in range(self.t_times)]
            # we call balanced performance to the measure= 0.5*AUC + 0.5*(min(sensitivity, specificity))
            fileAux2.write(''.join(["\n\n....Balanced performance (sensitivity and specificity) - rows are seeds and columns are thresholds : "]))
            fileAux2.write(''.join(["\n\n"]))
            for pp in range(0,self.t_times):
                for p in range(0,len(self.threshold)):
                    toPrint=0.5*medidas_AUC_threshold_across_folds[pp][p]+0.5*min(medidas_Sensi_threshold_across_folds[pp][p],medidas_Speci_threshold_across_folds[pp][p])
                    fileAux2.write(''.join(["\t",str(toPrint)]))  
                    medidas_AUC_comb_thresholds_across_folds[pp].append(toPrint)
                fileAux2.write(''.join(["\n"]))
        
        
            fileAux2.write(''.join(["\n\n....AUC  - rows are seeds and columns are thresholds : "]))
            fileAux2.write(''.join(["\n\n"]))
            for pp in range(0,self.t_times):
                for p in range(0,len(self.threshold)):
                    toPrint=medidas_AUC_threshold_across_folds[pp][p] 
                    fileAux2.write(''.join(["\t",str(toPrint)]))    
                fileAux2.write(''.join(["\n"]))
        
        
        # If i want to output kuncheva values for each seed and threshold. 
        #fileAux2.write(''.join(["\n\n....Kuncheva rows are thersholds and colomns are seeds : "]))
        #fileAux2.write(''.join(["\n\n"]))
        #for pp in range(0,self.t_times):
        #    for p in range(0,len(self.threshold)):
        #        toPrint=KunchevaIndexAcrossThresholdsAcrossTimes[pp][p] 
        #        fileAux2.write(''.join(["\t",str(toPrint)]))    
        #    fileAux2.write(''.join(["\n"]))
        
         
            file.write(''.join(["\n\n\n\n ------------------------ Results for all seeds ",str(self.t_times), " Repetitions (Seeds) ------------------ "]))
        
            # aggregator  (aggregates rankings from multiple seeds (already combined into a final one from multille FS methods and folds))
            if self.agregattor == "rank_mean":
                featureRankingAcrossTimes=MeanRankAgregattor(self.featuresrankTimes)
                featureRankPosAcrossTimes=SumRankPosAcrossFolds(featuresRankPosTimes)
                featureScoresAcrossTimes=MeanWeightAgregattor(self.featuresscoresTimes)[1]
            elif self.agregattor == "weight_mean":
                featureRankingAcrossTimes=MeanWeightAgregattor(self.featuresscoresTimes)[0]
                featureScoresAcrossTimes=MeanWeightAgregattor(self.featuresscoresTimes)[1]
            elif self.agregattor == "rank_min":
                featureRankingAcrossTimes=MinRankAgregattor(self.featuresrankTimes)
                featureScoresAcrossTimes=MinWeightAgregattor(self.featuresscoresTimes)[1]
            elif self.agregattor == "weight_min":
                featureRankingAcrossTimes=MinWeightAgregattor(self.featuresscoresTimes)[0]
                featureScoresAcrossTimes=MinWeightAgregattor(self.featuresscoresTimes)[1]
            elif self.agregattor == "rank_max":
                featureRankingAcrossTimes=MaxRankAgregattor(self.featuresrankTimes)
                featureScoresAcrossTimes=MaxWeightAgregattor(self.featuresscoresTimes)[1]
            elif self.agregattor == "weight_max":
                featureRankingAcrossTimes=MaxWeightAgregattor(self.featuresscoresTimes)[0]
                featureScoresAcrossTimes=MaxWeightAgregattor(self.featuresscoresTimes)[1]

        
            # Average ranking position of each feature over the fold*seeds - to compute individual worth
            featureRankPosAcrossTimes_averaged=featureRankPosAcrossTimes/(self.n_folds*  self.t_times)
           
            OrderedfeatureRankingPosAcrossTimes=getPositionOneArrayInAnother(featureRankingAcrossTimes,featureRankPosAcrossTimes_averaged)
    

            # Average across seeds of the stability achieved by each FS algorithm across the cross validation folds, per threshold
            KunchevaSameFSMethodFisherTotal=computeMeanColumnsOfNonEmptyNestedList(kunchevaSameFSMethodAcrossTimesFisher)
            KunchevaSameFSMethodChiSqTotal=computeMeanColumnsOfNonEmptyNestedList(kunchevaSameFSMethodAcrossTimesChiSq)
            KunchevaSameFSMethodTScoreTotal=computeMeanColumnsOfNonEmptyNestedList(kunchevaSameFSMethodAcrossTimesTScore)
            KunchevaSameFSMethodReliefTotal=computeMeanColumnsOfNonEmptyNestedList(kunchevaSameFSMethodAcrossTimesRelief)
            KunchevaSameFSMethodSVMRFATotal=computeMeanColumnsOfNonEmptyNestedList(kunchevaSameFSMethodAcrossTimesSVMRFA)
            KunchevaSameFSMethodMIMTotal=computeMeanColumnsOfNonEmptyNestedList(kunchevaSameFSMethodAcrossTimesMIM)
            KunchevaSameFSMethodCMIMTotal=computeMeanColumnsOfNonEmptyNestedList(kunchevaSameFSMethodAcrossTimesCMIM)
            KunchevaSameFSMethodMRMRTotal=computeMeanColumnsOfNonEmptyNestedList(kunchevaSameFSMethodAcrossTimesMRMR)
            KunchevaSameFSMethodSLL21Total=computeMeanColumnsOfNonEmptyNestedList(kunchevaSameFSMethodAcrossTimesLL21)
        
            fileAux.write(''.join(["\n\n\n Stability of each FS method individually across all seeds/Time: "]))
            fileAux.write(''.join(["\n\n Fisher:", "mean and std: ", str(np.mean(KunchevaSameFSMethodFisherTotal)), " ", str(np.std(KunchevaSameFSMethodFisherTotal)), str(KunchevaSameFSMethodFisherTotal), "\n ChiSquared:"," mean and std: ", str(np.mean(KunchevaSameFSMethodChiSqTotal)), " ", str(np.std(KunchevaSameFSMethodChiSqTotal)), str(KunchevaSameFSMethodChiSqTotal),"\n T-score:",str(KunchevaSameFSMethodTScoreTotal),"\n RelieF:"," mean and std: ", str(np.mean(KunchevaSameFSMethodReliefTotal)), " ", str(np.std(KunchevaSameFSMethodReliefTotal)),str(KunchevaSameFSMethodReliefTotal),"\n SVMRFA:",str(np.mean(KunchevaSameFSMethodSVMRFATotal)), " ", str(np.std(KunchevaSameFSMethodSVMRFATotal)),str(KunchevaSameFSMethodSVMRFATotal),"\n MIM:"," mean and std: ", str(np.mean(KunchevaSameFSMethodMIMTotal)), " ", str(np.std(KunchevaSameFSMethodMIMTotal)),str(KunchevaSameFSMethodMIMTotal),"\n CMIM:"," mean and std: ", str(np.mean(KunchevaSameFSMethodCMIMTotal)), " ", str(np.std(KunchevaSameFSMethodCMIMTotal)),str(KunchevaSameFSMethodCMIMTotal),"\n MRMR:"," mean and std: ", str(np.mean(KunchevaSameFSMethodMRMRTotal)), " ", str(np.std(KunchevaSameFSMethodMRMRTotal)),str(KunchevaSameFSMethodMRMRTotal),"\n LL21:"," mean and std: ", str(np.mean(KunchevaSameFSMethodSLL21Total)), " ", str(np.std(KunchevaSameFSMethodSLL21Total)),str(KunchevaSameFSMethodSLL21Total)]))
        
        
            # ------------------ Find the optimal threshold using RPT with different betas and AUC and AUCComb
        
            auxiliarKuncheva= np.mean(KunchevaIndexAcrossThresholdsAcrossTimes,axis=0)
            # compute the balanced AUC below:
            medidasCOMB_AUC_threshold_across_folds_times=[] # keeps the balanced AUC computed in the row below
            medidasCOMB_AUC_threshold_across_folds_times=performanceBalanced(medidas_AUC_threshold_across_folds_times,medidas_Sensi_threshold_across_folds_times,medidas_Speci_threshold_across_folds_times)

            beta=0.1
            RPTAcrossThresholdsAcrossTimes01=[]
            RPTCombAcrossThresholdsAcrossTimes01=[]
        
            for q in range(len(medidas_AUC_threshold_across_folds_times)):
                RPTAcrossThresholdsAcrossTimes01.append(RPT(medidas_AUC_threshold_across_folds_times[q],auxiliarKuncheva[q],beta)) # using AUC to measure performance
                RPTCombAcrossThresholdsAcrossTimes01.append(RPT(medidasCOMB_AUC_threshold_across_folds_times[q],auxiliarKuncheva[q],beta)) # using AUC comb to measure performance
        
            indexAUCComb01, valueAUCComb01 = max(enumerate(RPTCombAcrossThresholdsAcrossTimes01), key=operator.itemgetter(1))
            bestThresholdAUCComb01=self.threshold[indexAUCComb01]
        
            indexAUC01, valueAUC01 = max(enumerate(RPTAcrossThresholdsAcrossTimes01), key=operator.itemgetter(1))
            bestThresholdAUC01=self.threshold[indexAUC01]
        
            Grid_AUCcomb = 0.5 * medidas_AUC_threshold_across_folds_times[indexAUCComb01] + 0.5 * min(medidas_Sensi_threshold_across_folds_times[indexAUCComb01],medidas_Speci_threshold_across_folds_times[indexAUCComb01])
            
            gridSearchAUCComb[c][0].append(Grid_AUCcomb)
            gridSearch[c][0].append(valueAUCComb01)

            
            ind = indexAUCComb01 # index best threshold
            fileAux3.write(''.join(["\n ---------- RPT Beta = 0.1 : "]))
            fileAux3.write(''.join(["\n RPT (Balanced AUC): ","\t",  str(valueAUCComb01)]))
            fileAux3.write(''.join(["\n Comb AUC : ", "\t", str(np.mean(medidas_AUC_comb_thresholds_across_folds,axis=0)[ind]), "\t",  'std: ', "\t", str(np.std(medidas_AUC_comb_thresholds_across_folds,axis=0)[ind])]))
            fileAux3.write(''.join(["\n AUC : ", "\t", str(np.mean(medidas_AUC_threshold_across_folds,axis=0)[ind]), "\t",  'std: ', "\t", str(np.std(medidas_AUC_threshold_across_folds,axis=0)[ind])]))
            fileAux3.write(''.join(["\n Sensitivity : ","\t", str(np.mean(medidas_Sensi_threshold_across_folds,axis=0)[ind]), "\t",  'std: ', "\t", str(np.std(medidas_Sensi_threshold_across_folds,axis=0)[ind])]))
            fileAux3.write(''.join(["\n Specificity : ","\t", str(np.mean(medidas_Speci_threshold_across_folds,axis=0)[ind]), "\t",  'std: ', "\t", str(np.std(medidas_Speci_threshold_across_folds,axis=0)[ind])]))
            fileAux3.write(''.join(["\n Stability : ","\t", str(np.mean(KunchevaIndexAcrossThresholdsAcrossTimes,axis=0)[ind]), "\t",  'std: ', "\t", str(np.std(KunchevaIndexAcrossThresholdsAcrossTimes,axis=0)[ind])]))
            fileAux3.write(''.join(["\n No. of features : ", "\t", str(bestThresholdAUCComb01)]))            


            beta=1 
            RPTAcrossThresholdsAcrossTimes1=[]
            RPTCombAcrossThresholdsAcrossTimes1=[]

            for q in range(len(medidas_AUC_threshold_across_folds_times)):
                RPTAcrossThresholdsAcrossTimes1.append(RPT(medidas_AUC_threshold_across_folds_times[q],auxiliarKuncheva[q],beta))
                RPTCombAcrossThresholdsAcrossTimes1.append(RPT(medidasCOMB_AUC_threshold_across_folds_times[q],auxiliarKuncheva[q],beta))
        
            indexAUCComb1, valueAUCComb1 = max(enumerate(RPTCombAcrossThresholdsAcrossTimes1), key=operator.itemgetter(1))
            bestThresholdAUCComb1=self.threshold[indexAUCComb1]
        
            indexAUC1, valueAUC1 = max(enumerate(RPTAcrossThresholdsAcrossTimes1), key=operator.itemgetter(1))
            bestThresholdAUC1=self.threshold[indexAUC1]
        
            Grid_AUCcomb = 0.5 * medidas_AUC_threshold_across_folds_times[indexAUCComb1] + 0.5 * min(medidas_Sensi_threshold_across_folds_times[indexAUCComb1],medidas_Speci_threshold_across_folds_times[indexAUCComb1])
            
            gridSearchAUCComb[c][1].append(Grid_AUCcomb)
            gridSearch[c][1].append(valueAUCComb1)
            
            ind = indexAUCComb1 # index best threshold
            fileAux3.write(''.join(["\n ---------- RPT Beta = 1 : "]))
            fileAux3.write(''.join(["\n RPT (CombAUC): ","\t",  str(valueAUCComb1)]))
            fileAux3.write(''.join(["\n Balanced AUC : ", "\t", str(np.mean(medidas_AUC_comb_thresholds_across_folds,axis=0)[ind]), "\t",  'std: ', "\t", str(np.std(medidas_AUC_comb_thresholds_across_folds,axis=0)[ind])]))
            fileAux3.write(''.join(["\n AUC : ", "\t", str(np.mean(medidas_AUC_threshold_across_folds,axis=0)[ind]), "\t",  'std: ', "\t", str(np.std(medidas_AUC_threshold_across_folds,axis=0)[ind])]))
            fileAux3.write(''.join(["\n Sensitivity : ","\t", str(np.mean(medidas_Sensi_threshold_across_folds,axis=0)[ind]), "\t",  'std: ', "\t", str(np.std(medidas_Sensi_threshold_across_folds,axis=0)[ind])]))
            fileAux3.write(''.join(["\n Specificity : ","\t", str(np.mean(medidas_Speci_threshold_across_folds,axis=0)[ind]), "\t",  'std: ', "\t", str(np.std(medidas_Speci_threshold_across_folds,axis=0)[ind])]))
            fileAux3.write(''.join(["\n Stability : ","\t", str(np.mean(KunchevaIndexAcrossThresholdsAcrossTimes,axis=0)[ind]), "\t",  'std: ', "\t", str(np.std(KunchevaIndexAcrossThresholdsAcrossTimes,axis=0)[ind])]))
            fileAux3.write(''.join(["\n No. of features : ", "\t", str(bestThresholdAUCComb1)]))            

            
            beta=10
            RPTAcrossThresholdsAcrossTimes10=[] #use AUC as the classification measure
            RPTCombAcrossThresholdsAcrossTimes10=[] #use the balanced AUC as the classification measure
       
            for q in range(len(medidas_AUC_threshold_across_folds_times)):
                RPTAcrossThresholdsAcrossTimes10.append(RPT(medidas_AUC_threshold_across_folds_times[q],auxiliarKuncheva[q],beta))
                RPTCombAcrossThresholdsAcrossTimes10.append(RPT(medidasCOMB_AUC_threshold_across_folds_times[q],auxiliarKuncheva[q],beta))
            
            indexAUCComb10, valueAUCComb10 = max(enumerate(RPTCombAcrossThresholdsAcrossTimes10), key=operator.itemgetter(1))
            bestThresholdAUCComb10=self.threshold[indexAUCComb10]
        
            indexAUC10, valueAUC10 = max(enumerate(RPTAcrossThresholdsAcrossTimes10), key=operator.itemgetter(1))
            bestThresholdAUC10=self.threshold[indexAUC10]
        
            Grid_AUCcomb = 0.5 * medidas_AUC_threshold_across_folds_times[indexAUCComb10] + 0.5 * min(medidas_Sensi_threshold_across_folds_times[indexAUCComb10],medidas_Speci_threshold_across_folds_times[indexAUCComb10])
            
            gridSearchAUCComb[c][2].append(Grid_AUCcomb)
            gridSearch[c][2].append(valueAUCComb10)

            ind = indexAUCComb10 # index best threshold
            fileAux3.write(''.join(["\n ---------- RPT Beta = 10 : "]))
            fileAux3.write(''.join(["\n RPT (CombAUC): ","\t",  str(valueAUCComb10)]))
            fileAux3.write(''.join(["\n Balanced AUC : ", "\t", str(np.mean(medidas_AUC_comb_thresholds_across_folds,axis=0)[ind]), "\t",  'std: ', "\t", str(np.std(medidas_AUC_comb_thresholds_across_folds,axis=0)[ind])]))
            fileAux3.write(''.join(["\n AUC : ", "\t", str(np.mean(medidas_AUC_threshold_across_folds,axis=0)[ind]), "\t",  'std: ', "\t", str(np.std(medidas_AUC_threshold_across_folds,axis=0)[ind])]))
            fileAux3.write(''.join(["\n Sensitivity : ","\t", str(np.mean(medidas_Sensi_threshold_across_folds,axis=0)[ind]), "\t",  'std: ', "\t", str(np.std(medidas_Sensi_threshold_across_folds,axis=0)[ind])]))
            fileAux3.write(''.join(["\n Specificity : ","\t", str(np.mean(medidas_Speci_threshold_across_folds,axis=0)[ind]), "\t",  'std: ', "\t", str(np.std(medidas_Speci_threshold_across_folds,axis=0)[ind])]))
            fileAux3.write(''.join(["\n Stability : ","\t", str(np.mean(KunchevaIndexAcrossThresholdsAcrossTimes,axis=0)[ind]), "\t",  'std: ', "\t", str(np.std(KunchevaIndexAcrossThresholdsAcrossTimes,axis=0)[ind])]))
            fileAux3.write(''.join(["\n No. of features : ", "\t", str(bestThresholdAUCComb10)]))            


            # Find the optimal threshold across Times/seeds based (only) on AUC
            indexAUC, valueAUC = max(enumerate(medidas_AUC_threshold_across_folds_times), key=operator.itemgetter(1))
            bestThresholdAUC=self.threshold[indexAUC]
        
            # Find the optimal threshold across Times/seeds based on balanced AUC (COMBO)
            indexAUCCom, valueAUCCom = max(enumerate(medidasCOMB_AUC_threshold_across_folds_times), key=operator.itemgetter(1))
            bestThresholdAUCComb=self.threshold[indexAUCCom]
        
            # Find the optimal threshold across Times/seeds based on Stability
            indexSta, valueSta = max(enumerate(np.mean(KunchevaIndexAcrossThresholdsAcrossTimes,axis=0)), key=operator.itemgetter(1))
            bestThresholdSta=self.threshold[indexSta]
        
            # Find the optimal threshold across Times/seeds based on RPT + F-measure (beta =10)
            RPTAcrossThresholdsAcrossTimesRPTFM=[]
            for q in range(len(medidas_FMeasure_threshold_across_folds_times)):
                RPTAcrossThresholdsAcrossTimesRPTFM.append(RPT(medidas_FMeasure_threshold_across_folds_times[q],auxiliarKuncheva[q],beta))
            indexRPTFM, valueRPTFM = max(enumerate(RPTAcrossThresholdsAcrossTimesRPTFM), key=operator.itemgetter(1))
            bestThresholdRPTFM=self.threshold[indexRPTFM]
        
            # Find the optimal threshold across Times/seeds based on RPT + sensitivity (beta =10)
            RPTAcrossThresholdsAcrossTimesSensi=[]
            for q in range(len(medidas_Sensi_threshold_across_folds_times)):
                RPTAcrossThresholdsAcrossTimesSensi.append(RPT(medidas_Sensi_threshold_across_folds_times[q],auxiliarKuncheva[q],beta))
            indexRPTSensi, valueRPTSensi = max(enumerate(RPTAcrossThresholdsAcrossTimesSensi), key=operator.itemgetter(1))
            bestThresholdRPTSensi=self.threshold[indexRPTSensi]        
        

            file.write(''.join(["\n\nFeature ranking across repetitions*CV folds: \n",''.join(str(featureRankingAcrossTimes))])) 
            file.write(''.join(["\n\nFeature ranking across repetitions*CV folds (name): \n ",' '.join(self.dados.X.columns[featureRankingAcrossTimes].get_values())]))
        
            featuresScoresOrder = []
            for i in featureRankingAcrossTimes:
                featuresScoresOrder.append(featureScoresAcrossTimes[i])
            
            #file.write(''.join(["\nFeature scores Across Times: \n",''.join(str(featureScoresAcrossTimes))]))
            file.write(''.join(["\n\nFeature scores across repetitions*CV folds (Rank mean): \n",''.join(str(featuresScoresOrder))]))        
            file.write(''.join(["\n\n Best threshold according to: AUC: ",str(bestThresholdAUC), ", RPT (beta= 10) with predictability given by balanced_AUC: ", str(bestThresholdAUCComb10), ", RPT (beta= 10) with predictability given by AUC: ", str(bestThresholdAUC10),", RPT (beta= 1) with predictability given by balanced_AUC: ", str(bestThresholdAUCComb1),", RPT (beta= 1) with predictability given by AUC: ", str(bestThresholdAUC1)," , RPT (beta= 0.1) with predictability given by balanced_AUC: ", str(bestThresholdAUCComb01),", RPT (beta= 0.1) with predictability given by AUC: ", str(bestThresholdAUC01), " to stability: ", str(bestThresholdSta), ", RPT (beta= 10) with predictability given by FMeasure: ", str(bestThresholdRPTFM), ", RPT (beta= 10) with predictability given by sensitivity: ", str(bestThresholdRPTSensi), ", and to Log2n " , str(int(round(math.log(self.dados.X.shape[1],2))))]))
        
            # According to the results, using RPT + balanced AUC and beta=10 seems to achived the best results, so we will report them 
            bestThreshold=self.threshold[indexAUCComb10]
            topFeaturesAcrossTimes = featureRankingAcrossTimes[0:bestThreshold]
            file.write(''.join(["\n\nFeature scores across repetitions*CV folds (Rank mean): \n",''.join(str(featuresScoresOrder))]))        
            file.write(''.join(["\n\n Best threshold according to... AUC: ",str(bestThresholdAUC), ", RPT (beta= 10) with predictability given by balanced_AUC: ", str(bestThresholdAUCComb10), ", RPT (beta= 10) with predictability given by AUC: ", str(bestThresholdAUC10),", RPT (beta= 1) with predictability given by balanced_AUC: ", str(bestThresholdAUCComb1),", RPT (beta= 1) with predictability given by AUC: ", str(bestThresholdAUC1)," , RPT (beta= 0.1) with predictability given by balanced_AUC: ", str(bestThresholdAUCComb01),", RPT (beta= 0.1) with predictability given by AUC: ", str(bestThresholdAUC01), " to stability: ", str(bestThresholdSta), ", RPT (beta= 10) with predictability given by FMeasure: ", str(bestThresholdRPTFM), ", RPT (beta= 10) with predictability given by sensitivity: ", str(bestThresholdRPTSensi), ", and to Log2n " , str(int(round(math.log(self.dados.X.shape[1],2))))]))        
            index=indexAUCComb10 # chosen as the best threshold RPT + balanced AUC, beta=10
            file.write(''.join(["\n\n mean and std - Performance with the Best threshold: " , "Kuncheva: ", str(np.mean(KunchevaIndexAcrossThresholdsAcrossTimes,axis=0)[index]), "\t", str(np.std(KunchevaIndexAcrossThresholdsAcrossTimes,axis=0)[index]), "\n", " AUC: " ,  str(np.mean(medidas_AUC_threshold_across_folds,axis=0)[index]), "\t",  str(np.std(medidas_AUC_threshold_across_folds,axis=0)[index]), "\n", " Sensitivity: ", str(np.mean(medidas_Sensi_threshold_across_folds,axis=0)[index]), "\t",  str(np.std(medidas_Sensi_threshold_across_folds,axis=0)[index]), "\n", "Specificity: ", str(np.mean(medidas_Speci_threshold_across_folds,axis=0)[index]), "\t", str(np.std(medidas_Speci_threshold_across_folds,axis=0)[index]) ]))
       
 
            if len(self.FSmethods)>0:            
                file.write(''.join(["\n\n Averaged Kuncheva and standard deviation Across Times and across thresholds: \n",''.join(str(np.mean(np.mean(KunchevaIndexAcrossThresholdsAcrossTimes,axis=0),axis=0))), "  and standard deviation: ", str(np.std(np.mean(KunchevaIndexAcrossThresholdsAcrossTimes,axis=0),axis=0)) ]))
                file.write(''.join(["\n\n Averaged Kuncheva Across Times for each threshold: \n",''.join(str(np.mean(KunchevaIndexAcrossThresholdsAcrossTimes,axis=0)))]))    
                file.write(''.join(["\n\n Averaged AUC and standard deviation Across Times and across thresholds: \n",''.join(str(np.mean(medidas_AUC_threshold_across_folds_times,axis=0))), "  and standard deviation: ", str(np.std(medidas_AUC_threshold_across_folds_times,axis=0)) ])) 
                file.write(''.join(["\n\n Averaged AUC Across Times for each threshold: \n",''.join(str(medidas_AUC_threshold_across_folds_times))]))    
                file.write(''.join(["\n\n Averaged F-measure Across Times for each threshold: \n",''.join(str(medidas_FMeasure_threshold_across_folds_times))]))
                file.write(''.join(["\n\n Averaged Sensitivity Across Times for each threshold: \n",''.join(str(medidas_Sensi_threshold_across_folds_times))]))
                file.write(''.join(["\n\n Averaged Specificity Across Times for each threshold: \n",''.join(str(medidas_Speci_threshold_across_folds_times))]))
                file.write(''.join(["\n\n Averaged F-measure across repetitions*CV folds for each threshold: \n",''.join(str(medidas_FMeasure_threshold_across_folds_times))]))
                file.write(''.join(["\n\n Averaged Sensitivity across repetitions*CV folds for each threshold: \n",''.join(str(medidas_Sensi_threshold_across_folds_times))]))
                file.write(''.join(["\n\n Averaged Specificity across repetitions*CV folds for each threshold: \n",''.join(str(medidas_Speci_threshold_across_folds_times))]))
                file.write(''.join(["\n\n RPT (beta= 10) with predictability given by AUC across repetitions*CV folds for each threshold: \n",''.join(str(RPTAcrossThresholdsAcrossTimes10))]))
                file.write(''.join(["\n\n RPT (beta= 10) with predictability given by balanced_AUC across repetitions*CV folds for each threshold: \n",''.join(str(RPTCombAcrossThresholdsAcrossTimes10))]))            
                file.write(''.join(["\n\n RPT (beta= 1) with predictability given by AUC across repetitions*CV folds for each threshold: \n",''.join(str(RPTAcrossThresholdsAcrossTimes1))]))
                file.write(''.join(["\n\n RPT (beta= 1) with predictability given by balanced_AUC across repetitions*CV folds for each threshold: \n",''.join(str(RPTCombAcrossThresholdsAcrossTimes1))]))
                file.write(''.join(["\n\n RPT (beta= 0.1) with predictability given by AUC across repetitions*CV folds for each threshold: \n",''.join(str(RPTAcrossThresholdsAcrossTimes01))]))
                file.write(''.join(["\n\n RPT (beta= 0.1) with predictability given by balanced_AUC across repetitions*CV folds for each threshold: \n",''.join(str(RPTCombAcrossThresholdsAcrossTimes01))]))
        
            
                printKunchecha=np.mean(KunchevaIndexAcrossThresholdsAcrossTimes,axis=0)
                fig2=plt.figure()
                plt.plot(self.threshold,printKunchecha,'kd', label="Stability Index")
                plt.plot(self.threshold,medidas_AUC_threshold_across_folds_times,'bo',label="AUC")
                plt.plot(self.threshold,medidas_Sensi_threshold_across_folds_times,'rs',label="Sensitivity")
                plt.plot(self.threshold,medidas_Speci_threshold_across_folds_times, 'g^',label="Specificity")
                plt.axvline(bestThresholdAUCComb01, color='y', linestyle=':',linewidth=1, label="RPT(0.1)")
                plt.axvline(bestThresholdAUCComb1, color='m', linestyle='-.',linewidth=1, label="RPT(1)")
                plt.axvline(bestThresholdAUCComb10, color='c', linestyle='--',linewidth=1, label="RPT(10)")
                plt.axis([0 , self.threshold[-1]+1, 0.5, 1])
                plt.ylabel('Stability and classification performance')
                plt.xlabel('No. of selected features (k)')
                plt.legend(bbox_to_anchor=(0., 1.06, 1., .102), loc="upper center", ncol=7, columnspacing=.5,handletextpad=0., borderaxespad=0.)
                plt.show()
                fig2.savefig(''.join(["Classification_PerformanceAUC",self.filename,self.textclassifier,".pdf"]))
           
                fig3, ax = plt.subplots()
                featureRankPosAcrossTimes_averaged=1-normalizeWeights(OrderedfeatureRankingPosAcrossTimes[0:bestThreshold])
                y= featureRankPosAcrossTimes_averaged
                N = len(y)
                x = range(N)
                width = 1/1.5
                ax.bar(x, y, width, color="blue")
                x_ticks_labels=self.dados.X.columns[topFeaturesAcrossTimes].get_values()
                
                ax.set_xticks(range(len(x_ticks_labels)))
                ax.set_xticklabels(x_ticks_labels, rotation='vertical')
                plt.gcf().subplots_adjust(bottom=0.35)
                plt.ylabel("Feature weight")
                plt.show()
                
                fig3.savefig(''.join(["IndividualPopularity",self.filename,self.textclassifier,".pdf"]))        
        
        maxClassifier=[]
        maxClassifierAUCComb=[]
        for cl in range (len(gridSearch)):
            maxClassifier.append(np.max(gridSearch[cl]))
            maxClassifierAUCComb.append(np.max(gridSearchAUCComb[cl]))
        
        bestValue=np.max(maxClassifier)    
        bestClassifier= self.classifiers[np.argmax(maxClassifier)]
        fileAux3.write(''.join(["\n\n Best Classifier according to RPT : ", str(bestClassifier) , ' with RPT: ', str(bestValue) ]))
        
        bestValue=np.max(maxClassifierAUCComb)    
        bestClassifier= self.classifiers[np.argmax(maxClassifierAUCComb)]
        fileAux3.write(''.join(["\n\n Best Classifier according to AUC comb : ", str(bestClassifier) , ' with balanced AUC: ', str(bestValue) ]))
                
                    
                
                
                