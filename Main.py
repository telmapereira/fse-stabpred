#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 31 18:21:10 2017

@author: TelmaPereira and FranciscoFerreira
"""

from Ensemble import ensembleFS
from EnsembleGridSearch import ensembleFS_GridSearchClassifiers
from Dados import Dados
from Agreggators import MeanRankAgregattor
import numpy
numpy.set_printoptions(threshold=numpy.nan)
from Stability import performanceBalanced
import matplotlib.pyplot as plt
import pandas



print(" Loading data... ")
dados = Dados( "ADNI" , 4 ) # input - (dataset, time window)

print(" Defining number of features to be tested (1 to total number of features): ")
threshold_ADNI = Dados.setThreshold(dados)


print(" Run FSE-StabPred with a predefined classifier ... ")

# INPUT ensemble: (data, number of cross-validation folds, number of repetitions (seeds), FS methods, threshold - number of feature to be kept in the final ranking, classifier, ranker aggregattor, name of output file, optimized threshold, whether to use smote (smote=1) or not (smote=0) )
# Run 5 filter and 1 embedded FS methods - 'reliefF','MIM','CMIM','chi_square','MRMR','LL21'

# ADNI
dados = Dados( "ADNI" , 4 )
ensemble = ensembleFS(dados,5,10,['reliefF','MIM','CMIM','chi_square','MRMR','LL21'],threshold_ADNI, classifier="NaiveBayes",agregattor="rank_mean",filename="ADNI_4Y_NOsmote",smote=0)
ensemble.train()

print(" Run ensemble-based appriach using FSE-StabPred wih different classifiers ... ")

dados = Dados( "ADNI" , 4 )
ensemble = ensembleFS_GridSearchClassifiers(dados,5,10,['reliefF','MIM','CMIM','chi_square','MRMR','LL21'],threshold_ADNI,classifiers=['SVM_poly','SVM_rbf','LogisticRegression','DecisionTree','NaiveBayes'],agregattor="rank_mean",filename="ADNI_4Y_NOsmote_Grid",smote=0)
ensemble.train()


print("------ FINITO ------")
















