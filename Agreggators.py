#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 31 17:42:45 2017

@author: TelmaPereira and FranciscoFerreira
"""
import numpy as np
from skfeature.function.similarity_based import fisher_score
from skfeature.function.statistical_based import chi_square
from skfeature.function.statistical_based import t_score
from skfeature.function.statistical_based import f_score
from skfeature.function.similarity_based import reliefF
import math

def normalizeWeights(weights):
    return (weights-min(weights))/(max(weights)-min(weights))
    
    

def MeanRankAgregattor(ranks):
    
    rankTotal = np.zeros(ranks[0].size)

    for i in range(0,len(ranks)):
        for j in range(0,len(ranks[0])):
            rankTotal[ranks[i][j]]=rankTotal[ranks[i][j]]+j
        
    idx = np.argsort(rankTotal)

    return idx

def getPositionOneArrayInAnother(ranks,rankP):
    
    rankTotal = np.zeros(len(ranks))

    for i in range(len(ranks)):
        rankTotal[i]=rankP[ranks[i]]
        
    return rankTotal

def MeanRankAgregattorScore(ranks):
    
    rankTotal = np.zeros(ranks[0].size)

    for i in range(0,len(ranks)):
        for j in range(0,len(ranks[0])):
            rankTotal[ranks[i][j]]=rankTotal[ranks[i][j]]+j

    return rankTotal

def SumRankPosAcrossFolds(ranksP):
    
    rankPTotal = np.zeros(ranksP[0].size)

    for i in range(0,len(ranksP)):
        for j in range(0,len(ranksP[0])):
            rankPTotal[j]=rankPTotal[j]+ranksP[i][j]

    return rankPTotal    
    

def MinRankAgregattor(ranks):
    
    rankTotal = np.ones(ranks[0].size)*(ranks[0].size+2)
    
    for i in range(0,len(ranks)):
        for j in range(0,len(ranks[0])):
            rankTotal[ranks[i][j]]=min(rankTotal[ranks[i][j]],j)
                    
    idx = np.argsort(rankTotal)
    
    return idx
    
def MaxRankAgregattor(ranks):
    
    rankTotal = np.zeros(ranks[0].size)
    
    for i in range(0,len(ranks)):
        for j in range(0,len(ranks[0])):
            rankTotal[ranks[i][j]]=max(rankTotal[ranks[i][j]],j)
                    
    idx = np.argsort(rankTotal)
    
    return idx
    


def MeanWeightAgregattor(weights):
    
    total = np.zeros(len(weights[0]))
    for i in range(len(weights)):
        total = total + normalizeWeights(weights[i])

    
    #total=total/len(weights[0])
    t_weigths = normalizeWeights(total)
    total=total*-1
    total = np.argsort(total)

    return [total,t_weigths]

def GeomMeanWeightAgregattor(weights):
    
    total = np.ones(len(weights[0]))
    for i in range(len(weights)):
        total = total * normalizeWeights(weights[i])

    
    total=[math.pow(total[j],1/len(weights)) for j in range(len(total))]
    
    total = np.argsort(total)
    total=np.fliplr([total])[0]
    
    return total


def MinWeightAgregattor(weights):
    
    weightMatrix = np.zeros([len(weights),len(weights[0])])
    for i in range(len(weights)):
        weightMatrix[i,:]=normalizeWeights(weights[i])
        
    total=np.amin(weightMatrix,axis=0)
    t_weigths = total
    total=total*-1
    total = np.argsort(total)
    
    return [total,t_weigths]


def MaxWeightAgregattor(weights):  
    weightMatrix = np.zeros([len(weights),len(weights[0])])
    for i in range(len(weights)):
        weightMatrix[i,:]=normalizeWeights(weights[i])
        
    total=np.amax(weightMatrix,axis=0)
    t_weigths = total
    total=total*-1
    total = np.argsort(total)
    
    return [total,t_weigths]

