#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug  1 16:27:16 2017

@author: TelmaPereira and FranciscoFerreira
"""
import numpy as np

def calcSensiSpeci(cm):
    
    values = [0,0]
    values[0] = cm[0,0]/(cm[0,0]+cm[0,1])
    values[1] = cm[1,1]/(cm[1,0]+cm[1,1])
         
    return values

# check if a list is empty
def empty(seq):
    try:
        return all(map(empty, seq))
    except TypeError:
        return False

# Check first if the nested list is empty and if not, compute the mean across columns
def computeMeanColumnsOfNonEmptyNestedList(nestedList):
    
    if empty(nestedList):
        # if the list is empty do nothing
        return 0
    else:
        listTotal=[]
        listTotal=np.mean(nestedList,axis=0)
        return listTotal
    

def transformWeight(weights,rank):
    t_weights = np.ones(len(rank))
    
    for i in range(len(rank)):
        t_weights[rank[i]]=weights[i]
        
    return t_weights
    
