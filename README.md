** FSE-StabPred **


## Description
FSE-StabPred is a feature selection (FS) ensemble combining stability and predictability to choose the most relevant NPTs for prognostic prediction in AD. First, we combine the outcome of multiple (filter and embedded) FS methods. Then, we use a wrapper-based approach optimizing both stability and predictability to compute the number of selected features. More details may be found in the paper: "Neuropsychological predictors of conversion from Mild Cognitive Impairment to Alzheimer?s Disease: A feature selection ensemble combining stability and predictability." 
The optimal subset of features can vary with the classifier used to assess predictability. In this context, the proposed FS ensemble can be (optionally) run with multiple classifiers in an ensemble-based approach. The emergent subsets of features are then combined in the aggregator, which selects the pair of features and classifier that yields the highest classification and/or stability performance. 


---
## Steps

### Prerequisites:
scikit-feature repository (http://featureselection.asu.edu)

Python 2.7 *and Python 3*

NumPy

SciPy

Scikit learn

### How to use:
 Follow instructions in the Main.py. 

---
## Citations

If you find FSE-StabPred useful in your research, please consider citing the following paper:

@Article{Pereira2018,

title="Neuropsychological predictors of conversion from mild cognitive impairment to Alzheimer's disease: a feature selection ensemble combining stability and predictability",

author="Pereira, Telma and Ferreira, Francisco L. and Cardoso, Sandra and Silva, Dina and de Mendon{\c{c}}a, Alexandre
and Guerreiro, Manuela and Madeira, Sara C. and {for the Alzheimer's Disease Neuroimaging Initiative}",

journal="BMC Medical Informatics and Decision Making",

year="2018",
volume="18",
number="1",
pages="137",
}


---
## Contact
Telma Pereira

E-mail: telma.pereira@tecnico.ulisboa.pt 